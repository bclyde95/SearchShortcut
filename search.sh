#/bin/bash

# search urls
bing=https://www.bing.com/search?q=
youtube=https://www.youtube.com/results?search_query=

# If one argument
if [ $# -eq 1 ] ; then
    # Convert spaces to '+' in the query and send it to the search log
    query=$(echo $1 | sed -e 's/\s/+/g')
    echo "$(date +"%D %T"): $query" >> /home/brandon/ConfigRef/searchShortcut/searches.log

    # Check if firefox or opera are running, if so run the search in one of those.
    if [[ $(ps -e | grep firefox) ]] ; then
        # send the firefox call to the background and kill any output
        nohup firefox $bing$query > /dev/null 2>&1
        echo "Searching '$1' in Firefox"
    elif [[ $(ps -e | grep opera) ]] ; then
        nohup opera $bing$query > /dev/null 2>&1
        echo "Searching '$1' in Opera"

    # If the one arg is '-f', open bing in firefox. Otherwise, open bing in opera
    elif [[ $1 == "-f" ]] ; then
        nohup firefox https://www.bing.com > /dev/null 2>&1 &
        echo "Opening Bing in Firefox"
    else
        nohup opera $bing$query > /dev/null 2>&1 &
        echo "Opening Opera and searching '$1'"
    fi

# If two arguments
elif [ $# -eq 2 ] ; then
    
    # Convert spaces to '+' in the query and send it to the search log
    query=$(echo $2 | sed -e 's/\s/+/g')
    echo "$(date +"%D %T"): $query" >> /home/brandon/ConfigRef/searchShortcut/searches.log

    # if the first arg is '-o', send the query to opera. If opera is running send it to that process, otherwise open a new instance of opera
    if [ $1 == "-o" ] ; then
        if [[ $(ps -e | grep opera) ]] ; then
            nohup opera $bing$query > /dev/null 2>&1
            echo "Searching '$2' in Opera"
        else
            nohup opera https://www.bing.com/search?q=$query > /dev/null 2>&1 &
            echo "Opening Opera and searching '$2'"
        fi
    # if the first arg is '-f', send the query to firefox. If firefox is running send it to that process, otherwise open a new instance of firefox
    elif [ $1 == "-f" ] ; then
        if [[ $(ps -e | grep firefox) ]] ; then
            nohup firefox $bing$query > /dev/null 2>&1
            echo "Searching '$2' in Firefox"
        else
            nohup firefox $bing$query > /dev/null 2>&1 &
            echo "Opening Firefox and searching '$2'"
        fi
    # if the first arg is '-y', check if firefox or opera are running. If so, send the query there. Otherwise, open opera and search
    elif [ $1 == "-y" ] ; then
        if [[ $(ps -e | grep firefox) ]] ; then
            nohup firefox $youtube$query > /dev/null 2>&1
            echo "Searching '$2' on Youtube in Firefox"
        elif [[ $(ps -e | grep opera) ]] ; then
            nohup opera $youtube$query > /dev/null 2>&1
            echo "Searching '$2' on Youtube in Opera"
        else
            nohup opera $youtube$query > /dev/null 2>&1 &
            echo "Opening Opera and searching '$2' on Youtube"
        fi

    else
        echo "Invalid args"
        echo "Wrap multi word searches in single or double quotes"
    
    fi

# If three arguments
elif [ $# -eq 3 ] ; then

    # Convert spaces to '+' in the query and send it to the search log
    query=$(echo $3 | sed -e 's/\s/+/g')
    echo "$(date +"%D %T"): $query" >> /home/brandon/ConfigRef/searchShortcut/searches.log

    # the only flag combo allowed with 3 args is '-y' with a browser selection. If different, throw an error 
    if [ $2 == "-y" ] ; then
        if [ $1 == "-o" ] ; then
            if [[ $(ps -e | grep opera) ]] ; then
                nohup opera $youtube$query > /dev/null 2>&1
                echo "Searching '$3' on Youtube in Opera"
            else
                nohup opera $youtube$query > /dev/null 2>&1 &
                echo "Opening Opera and searching '$3' on Youtube"
            fi
        elif [ $1 == "-f" ] ; then
            if [[ $(ps -e | grep firefox) ]] ; then
                nohup firefox $youtube$query > /dev/null 2>&1
                echo "Searching '$3' on Youtube in Firefox"
            else
                nohup firefox $youtube$query > /dev/null 2>&1 &
                echo "Opening Firefox and searching '$3' on Youtube"
            fi  
        else
            echo "Invalid arguments"
        fi
    else
        echo "Invalid arguments"
    fi

# throw error if arguments > 3
elif [ $# -gt 3 ] ; then
    echo "Too many arguments"
    echo "Wrap multi word searches in single or double quotes"

# if no argument is supplied, open bing in opera
else
    nohup opera https://www.bing.com > /dev/null 2>&1 &
    echo "Opening Bing in Opera"
fi
