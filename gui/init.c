#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

static void enter(GtkWidget *widget, gpointer data)
{
    char* script = "/home/brandon/ConfigRef/searchShortcut/search.sh ";
    char* command = (char *)malloc(sizeof(script)+sizeof(gtk_entry_get_text(GTK_ENTRY(data)))+3);

    sprintf(command, "/home/brandon/ConfigRef/searchShortcut/search.sh \"%s\"\0", gtk_entry_get_text(GTK_ENTRY(data)));

    system(command);

    free(command);
    exit(0);
}

static gboolean check_escape(GtkWidget *widget, GdkEventKey *event, gpointer data)
{
    if (event->keyval == GDK_KEY_Escape)
    {
        gtk_main_quit();
        return TRUE;
    }
    return FALSE;
}

int main(int argc, char **argv)
{
    gtk_init(&argc, &argv);

    GtkWidget *window, *entry, *box;

    GtkCssProvider* provider = gtk_css_provider_new();
    gtk_css_provider_load_from_path(GTK_CSS_PROVIDER(provider), "/home/brandon/ConfigRef/searchShortcut/gui/Styles.css", NULL);
        
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 100);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    
    entry = gtk_entry_new();
    g_signal_connect(entry, "activate", G_CALLBACK(enter), entry);
    gtk_entry_set_alignment(GTK_ENTRY(entry), 0.5);

    GtkStyleContext* context = gtk_widget_get_style_context(entry);
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_USER);


    gtk_container_add(GTK_CONTAINER(window), entry);

    g_signal_connect(window, "key_press_event", G_CALLBACK(check_escape), NULL);

    gtk_window_set_type_hint(GTK_WINDOW(window), GDK_WINDOW_TYPE_HINT_DIALOG);
    gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
