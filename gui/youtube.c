#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

const int WINDOW_WIDTH = 1000;
const int WINDOW_HEIGHT = 100;

/* Handle Enter Keypress, builds command and runs it */
static void enter(GtkWidget *widget, gpointer data)
{
    // Set script variable and use it to allocate memory for the command
    char* script = "/home/brandon/ConfigRef/searchShortcut/search.sh ";
    char* command = (char *)malloc(sizeof(script)+sizeof(gtk_entry_get_text(GTK_ENTRY(data)))+6);

    // Build command
    sprintf(command, "%s-y \"%s\"\0", script, gtk_entry_get_text(GTK_ENTRY(data)));

    //strcat(command, "-y ");
    //strcat(command, gtk_entry_get_text(GTK_ENTRY(data)));

    // Execute command
    system(command);

    // Free memory allocation
    free(command);
    exit(0);
}

/* Handle Escape Keypress, exits program if pressed */
static gboolean check_escape(GtkWidget *widget, GdkEventKey *event, gpointer data)
{
    // If escape is pressed, quit program
    if (event->keyval == GDK_KEY_Escape)
    {
        gtk_main_quit();
        return TRUE;
    }
    return FALSE;
}

int main(int argc, char **argv)
{
    // Initialize GTK and declare widgets used
    gtk_init(&argc, &argv);

    GtkWidget *window, *entry, *box;

    // Load CSS styling
    GtkCssProvider* provider = gtk_css_provider_new();
    gtk_css_provider_load_from_path(GTK_CSS_PROVIDER(provider), "/home/brandon/ConfigRef/searchShortcut/gui/y-style.css", NULL);
    
    // Create window with specified width and height, and link exit function
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(window), 1000, 100);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    
    // Create input field and link enter callback
    entry = gtk_entry_new();
    g_signal_connect(entry, "activate", G_CALLBACK(enter), entry);
    gtk_entry_set_alignment(GTK_ENTRY(entry), 0.5);

    // Apply styling
    GtkStyleContext* context = gtk_widget_get_style_context(entry);
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_USER);

    // Add components to GUI
    gtk_container_add(GTK_CONTAINER(window), entry);

    // Link check_escape callback
    g_signal_connect(window, "key_press_event", G_CALLBACK(check_escape), NULL);

    // Enable GUI
    gtk_window_set_type_hint(GTK_WINDOW(window), GDK_WINDOW_TYPE_HINT_DIALOG);
    gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
