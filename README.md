# SearchShortcut

### Global search shortcut for Linux

#### Allows search in browser from anywhere on the desktop via simple GTK interface


## Installation
* Make sure requirements are installed
* Clone the repo


## Usage

### GUI
* Assign a keyboard shortcut or start from the command line
  * *search* for browser
  * *youtube* for YouTube

### CLI
* search.sh can be used directly

* Usage: search.sh "Search query"
    * Checks to see if a Opera or Firefox instance is running and searches in the first instance found
    * Default browser is Opera, add *-f* for firefox
    * Default search mode is browser, add *-y* for YouTube
    * If no argument is supplied, it will simply open Bing in Opera

## Requirements
* BASH
* Nohup
* Gtk3
* Opera
* Firefox
